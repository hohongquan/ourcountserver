var functions = require('firebase-functions');

// Start writing Firebase Functions
// https://firebase.google.com/functions/write-firebase-functions

exports.updateAmount = functions.database.ref('/groups/{groupId}')
    .onWrite(event => {
        const group = event.data;
        var groupId = event.params.groupId;

        const paymentsSnapshot= group.child("/payments");
        var total = 0; 

        paymentsSnapshot.forEach(function(paymentItemSnapshot){
            var childData = paymentItemSnapshot.val(); 
            total = total + parseInt(childData.amount)
        });  

        var isTotalChanged = false; 
        if(group.val().total != total){
            isTotalChanged = true; 
        }

        if (paymentsSnapshot.changed() && isTotalChanged) {
            console.log("update total", total); 
            return event.data.ref.update({
                total: total
            });
        }

    });